This repository holds the data and machine learning models to help
with GitLab issue triaging.

### Data

`data/` may contain encrypted data. Files ending with `.7z` are stored in
Git LFS and encrypted with a password in the CI/CD variables.

For example, to extract `data/gitlab-issues.7z`, run:

```sh
7z x data/gitlab-issues.7z
```
