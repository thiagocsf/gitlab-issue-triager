from __future__ import print_function

import sys
from keras.preprocessing import sequence
from keras.models import Sequential
from keras.layers import Dense, Embedding
from keras.layers import LSTM

import data_helper
from sklearn.model_selection import train_test_split


max_features = 20000
maxlen = 100
batch_size = 10

print("loading data ... ")
input_file = sys.argv[1]
x_, y_, vocabulary, vocabulary_inv, df, labels = data_helper.load_data(input_file)
# Split the original dataset into train set and test set
x, x_test, y, y_test = train_test_split(x_, y_, test_size=0.1)

# Split the train set into train set and dev set
x_train, x_dev, y_train, y_dev = train_test_split(x, y, test_size=0.1)

print('Build model...')
model = Sequential()
model.add(Embedding(max_features, 256))
model.add(LSTM(128, dropout=0.2, recurrent_dropout=0.2))
model.add(Dense(1, activation='sigmoid'))

# try using different optimizers and different optimizer configs
model.compile(loss='categorical_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])

print('Train...')
model.fit(x_train, y_train,
          batch_size=batch_size,
          epochs=15,
          validation_data=(x_test, y_test))
score, acc = model.evaluate(x_test, y_test,
                            batch_size=batch_size)
print('Test score:', score)
print('Test accuracy:', acc)
