import tensorflow as tf
import numpy as np

from bs4 import BeautifulSoup
from collections import namedtuple

from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences

from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize

import json
import os
import re
import requests
import sys

SLACK_URL = os.environ.get('SLACK_URL', None)
PRODUCTION_MODE = os.environ.get('PRODUCTION_MODE', False) == "1"
PRIVATE_TOKEN = os.environ.get('PRIVATE_TOKEN', None)

if PRODUCTION_MODE and PRIVATE_TOKEN is None:
    print('PRIVATE_TOKEN is not set!')
    sys.exit(1)

model = load_model('../models/gitlab_issue_classify.h5')

with open('../models/text_tokenizer.json') as f:
    text_tokenizer = tf.keras.preprocessing.text.tokenizer_from_json(f.read())

with open('../models/label_tokenizer.json') as f:
    label_tokenizer = tf.keras.preprocessing.text.tokenizer_from_json(f.read())

max_length = 8192
padding_type = 'post'
trunc_type = 'post'
STOP_WORDS = set(stopwords.words('english'))

def send_slack_message(message):
    if SLACK_URL is None:
        return True

    data = {'text': message, 'channel': '@stanhu', 'username': 'TanukiStan', 'icon_emoji': ':tanuki'}

    if PRODUCTION_MODE:
        data['channel'] = 'feed_tanuki-stan'

    headers = {'Content-Type': 'application/json'}
    resp = requests.post(SLACK_URL, headers=headers, data=json.dumps(data))
    print("Slack message response: %s" % resp)

def update_issue_with_note(issue, label, confidence):
    note = '''This issue was automatically tagged with the label ~"{label}" by TanukiStan, a machine learning classification model, with a probability of {confidence:.2g}.

If this label is incorrect, please tag this issue with the correct group label as well as ~"automation:ml wrong" to help me learn from my mistakes.
''' \
    .format(label=label, confidence=confidence)

#    print(note)

    labels = issue['labels'] + [label, "automation:ml"]
    iid = issue['iid']

    print("adding labels %s" % labels)

    if PRIVATE_TOKEN is None:
        return True

    post_note_url = "https://docker.for.mac.localhost:3001/api/v4/projects/1/issues/35/notes"
    put_issue_url = "https://docker.for.mac.localhost:3001/api/v4/projects/1/issues/35"

    if PRODUCTION_MODE:
        post_note_url = "https://gitlab.com/api/v4/projects/278964/issues/%d/notes" % iid
        put_issue_url = "https://gitlab.com/api/v4/projects/278964/issues/%d" % iid

    headers = {'PRIVATE-TOKEN': os.environ['PRIVATE_TOKEN'], 'Content-Type': 'application/json'}
    params = {'labels': ",".join(labels)}


    resp = requests.put(put_issue_url, params=params, headers=headers, verify=False)

    if resp.status_code != 200:
        print("Failed updating labels: %s" % resp)
        return False

    data = {'body': note}
    resp = requests.post(post_note_url, data=json.dumps(data), headers=headers, verify=False)

    if resp.status_code != 201:
        print("Failed creating new note: %s" % resp)
        return False

    return True

def predict_group(predict_msg):
    new_seq = text_tokenizer.texts_to_sequences(predict_msg)
    padded = pad_sequences(new_seq, maxlen=max_length,
                           padding=padding_type,
                           truncating=trunc_type)
    return (model.predict(padded))

def request_issues(page):
#    url = 'https://gitlab.com/api/v4/projects/278964/issues?state=opened&per_page=100&page=%d&label=Manage+[DEPRECATED]' % page
#    url = 'https://gitlab.com/api/v4/projects/278964/issues?state=opened&per_page=100&page=%d&labels=infradev' % page
#    url = 'https://gitlab.com/api/v4/projects/278964/issues?state=opened&per_page=100&created_after=2020-12-14T00:00Z&page=%d' % page
    url = 'https://gitlab.com/api/v4/projects/278964/issues?state=opened&per_page=100&created_after=2020-06-01T00:00Z&page=%d' % page
    print(url)
    resp = requests.get(url)

    unlabeled_issues = []

    for issue in resp.json():
        title = issue['title']
        description = issue['description']
        labels = issue['labels']

        lower_title = title.lower()
        if lower_title.find('live') >= 0 or lower_title.find('watch') >= 0 or lower_title.find('reddit') >= 0:
            print("Skipping potential spam: %s" % title)
            continue

        found = False
        for l in labels:
            if l.find('group::') == 0:
                found = True
                break

        if not found:
            text = "%s\n\n%s" % (title, description)
            text = BeautifulSoup(text, features="html.parser").get_text().lower()
            word_tokens = word_tokenize(text)
            filtered_sentence = [x for x in word_tokens if x not in STOP_WORDS and not x.isdigit()]
            text = ' '. join(filtered_sentence).lower()
            print(text)
            issue['text'] = text
            unlabeled_issues.append(issue)

    return unlabeled_issues

def tag_issues(page):
    unlabeled_issues = request_issues(page=page)

    if len(unlabeled_issues) == 0:
        print("No unlabeled issues")
        return

    predictions = predict_group([x['text'] for x in unlabeled_issues])

    label_word_index = label_tokenizer.word_index
    inv_dict = {v: k for k, v in label_word_index.items()}

    for i, result in enumerate(predictions):
        index = np.argmax(result)
        confidence = result[index]
        issue = unlabeled_issues[i]
        iid = issue['iid']
        label = inv_dict[index].replace('_', ' ')
        url = "https://gitlab.com/gitlab-org/gitlab/issues/%d" % iid
        if confidence > 0.20:
            message = ":green-circle-check: <%s|Issue #%d: %s>: :arrow_right: added label `%s` (probability %.2f)" % (url, iid, issue['title'], label, confidence)
            print(message)
            success = update_issue_with_note(issue, label, confidence)
            if success:
                send_slack_message(message)
        else:
            message = ":red_circle: <%s|Issue #%d: %s>: did not apply label `%s` (probability %.2f)" % (url, iid, issue['title'], label, confidence)
            print(message)
            send_slack_message(message)

tag_issues(1)
# TODO Adjust page number
# tag_issues(8)
